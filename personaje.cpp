#include <ncurses.h>
using namespace std;
void personaje(int y, int x){
    mvprintw(y,x,"0");
}
int main(){
    initscr();
    cbreak();
    noecho();
    keypad(stdscr,TRUE);
    int mv;
    int x = 5;
    int y = 5;
    while(TRUE){
        personaje(y,x);
        mv = getch();
        if(mv == KEY_UP){
            y--;
            personaje(y,x);
            refresh();
            clear();
        }
        if(mv == KEY_DOWN){
            y++;
            personaje(y,x);
            refresh();
            clear();

        }
         if(mv == KEY_RIGHT){
            x++;
            personaje(y,x);
            refresh();
            clear();

        }
         if(mv == KEY_LEFT){
            x--;
            personaje(y,x);
            refresh();
            clear();

        }
    }
    refresh();
    endwin();
    return 0;
}
