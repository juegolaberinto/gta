#include <ncurses.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>

using namespace std;
const char*lv = "|";
const char*lh = "-";
const char*C = "+";
void personaje(int y, int x){
    mvprintw(y,x,"0");
}
void mapa(){
    mvprintw (1,75,"Laberinto");
    mvprintw (3,55,"Para jugar utilize las flechas. Para reiniciar presione r.Para salir presione x");
    mvprintw(5,5,C);
    mvprintw(5,150,C);
    mvprintw(50,5,C);
    mvprintw(50,150,C);
    for ( int i = 6 ; i <= 49 ; i++){
    mvprintw(i,5,lv);
    }
    for ( int i = 6 ; i <=49 ; i++){
    mvprintw(i,150,lv);
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(5,i,lh);
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(50,i,lh);
    }

}
void mapa_I(){
    mvprintw(1,75,"Laberinto");
    mvprintw(3,55," Para jugar utilize las flechas. Para reiniciar el nivel presione r. Para salir presione x");
    mvprintw(5,5,"+");
    mvprintw(5,150,"+");
    mvprintw(50,5,"+");
    mvprintw(50,150,"+");
    for ( int i = 6 ; i <= 49 ; i++){
    mvprintw(i,5,lv);
    }
    for ( int i = 6 ; i <=49 ; i++){
    mvprintw(i,150,lv);
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(5,i,lh);
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(50,i,lh);
    }
// Lineas Horizontales
    for (int i =10; i < 60; i++){
        mvprintw(10,i,lh);
    }
    for (int i =6; i < 55; i++){
        mvprintw(15,i,lh);
    }
    for (int i =80; i < 115; i++){
        mvprintw(15,i,lh);
    }
    for (int i =90; i < 125; i++){
        mvprintw(20,i,lh);
    }
    for (int i =10; i < 70; i++){
        mvprintw(25,i,lh);
    }
    for (int i =20; i < 60; i++){
        mvprintw(30,i,lh);
    }
    for (int i =70; i < 80; i++){
        mvprintw(30,i,lh);
    }
    for (int i =25; i < 50; i++){
        mvprintw(35,i,lh);
    }
    for (int i =75; i < 120; i++){
        mvprintw(35,i,lh);
    }
    for (int i =125; i< 150;i++){
        mvprintw(35,i,lh);
    }
    for (int i =40; i < 75; i++){
        mvprintw(40,i,lh);
    }
    for (int i =90; i < 140; i++){
        mvprintw(40,i,lh);
    }
    for (int i =50; i < 75; i++){
        mvprintw(45,i,lh);
    }
    for (int i =85; i < 100; i++){
        mvprintw(45,i,lh);
    }
    for (int i =105; i < 149; i++){
        mvprintw(45,i,lh);
    }
//Lineas Verticales

    for (int i =16; i< 22; i++){
        mvprintw(i,10,lv);
    }
    for (int i =30; i< 50; i++){
        mvprintw(i,10,lv);
    }
    for (int i =36; i< 50; i++){
        mvprintw(i,25,lv);
    }
    for (int i =18; i< 25; i++){
        mvprintw(i,25,lv);
    }
    for (int i =16; i< 22; i++){
        mvprintw(i,55,lv);
    }
    for (int i =26; i< 40; i++){
        mvprintw(i,60,lv);
    }
    for (int i =6; i<25; i++){
        mvprintw(i,70,lv);
    }
    for (int i =6; i< 30; i++){
        mvprintw(i,80,lv);
    }
    for (int i =36; i< 40; i++){
        mvprintw(i,75,lv);
    }
    for (int i =45; i< 50; i++){
        mvprintw(i,75,lv);
    }
    for (int i =36; i< 45; i++){
        mvprintw(i,85,lv);
    }
    for (int i =6; i< 12; i++){
        mvprintw(i,90,lv);
    }
    for (int i =21; i< 30; i++){
        mvprintw(i,95,lv);
    }
    for (int i =10; i< 35; i++){
        mvprintw(i,125,lv);
    }
    for (int i =6; i< 30; i++){
        mvprintw(i,138,lv);
    }

}
int mapa_II(){
    mvprintw (3,75,"Laberinto");
    mvprintw(5,5,"+");
    mvprintw(5,150,"+");
    mvprintw(50,5,"+");
    mvprintw(50,150,"+");

    for ( int i = 6 ; i <= 49 ; i++){
    mvprintw(i,5,"|");
    }
    for ( int i = 6 ; i <=49 ; i++){
    mvprintw(i,150,"|");
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(5,i,"-");
    }
    for ( int i = 6 ; i <= 149 ; i++){
    mvprintw(50,i,"-");
    }

    for ( int i = 6 ; i <= 18 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 23 ; i <= 41 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 44 ; i <= 67 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 70 ; i <= 81 ; i++){
    mvprintw(7,i,"-");
    }
     for ( int i = 84 ; i <= 93 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 106 ; i <= 119 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 125 ; i <= 144 ; i++){
    mvprintw(7,i,"-");
    }
    for ( int i = 6 ; i <= 20 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 23 ; i <= 49 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 56 ; i <= 67 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 70 ; i <= 81 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 86 ; i <= 98 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 106 ; i <= 129 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 135 ; i <= 149 ; i++){
    mvprintw(10,i,"-");
    }
    for ( int i = 9 ; i <= 25 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 31 ; i <= 47 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 58 ; i <= 62 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 64 ; i <= 89 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 95 ; i <= 122 ; i++){
    mvprintw(13,i,"-");
    }
     for ( int i = 124 ; i <= 126 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 131 ; i <= 140 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 142 ; i <= 144 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 146 ; i <= 149 ; i++){
    mvprintw(13,i,"-");
    }
    for ( int i = 6 ; i <= 12 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 15 ; i <= 32 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 36 ; i <= 48 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 51 ; i <= 69 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 75 ; i <= 83 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 88 ; i <= 94 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 96 ; i <= 114 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 118 ; i <= 126 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 130 ; i <= 149 ; i++){
    mvprintw(16,i,"-");
    }
    for ( int i = 6 ; i <= 12 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 23 ; i <= 32 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 39 ; i <= 54 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 58 ; i <= 69 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 79 ; i <= 91 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 94 ; i <= 95 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 99 ; i <= 112 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 117 ; i <= 122 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 130 ; i <= 144 ; i++){
    mvprintw(19,i,"-");
    }
    for ( int i = 6 ; i <= 17 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 23 ; i <= 49 ; i++){
    mvprintw(22,i,"-");
    }
     for ( int i = 56 ; i <= 62 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 70 ; i <= 80 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 86 ; i <= 94 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 106 ; i <= 122 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 135 ; i <= 147 ; i++){
    mvprintw(22,i,"-");
    }
    for ( int i = 9 ; i <= 25 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 31 ; i <= 54 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 59 ; i <= 62 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 64 ; i <= 89 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 95 ; i <= 122 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 128 ; i <= 129 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 131 ; i <= 140 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 142 ; i <= 144 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 146 ; i <= 149 ; i++){
    mvprintw(25,i,"-");
    }
    for ( int i = 9 ; i <= 15 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 27 ; i <= 47 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 53 ; i <= 60 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 64 ; i <= 81 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 95 ; i <= 118 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 122 ; i <= 129 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 136 ; i <= 140 ; i++){
    mvprintw(28,i,"-");
    }
    for ( int i = 147 ; i <= 148 ; i++){
    mvprintw(28,i,"-");
    for ( int i = 6 ; i <= 20 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 23 ; i <= 51 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 56 ; i <= 67 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 70 ; i <= 81 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 86 ; i <= 98 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 106 ; i <= 129 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 135 ; i <= 149 ; i++){
    mvprintw(31,i,"-");
    }
    for ( int i = 6 ; i <= 23 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 28 ; i <= 47 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 53 ; i <= 64 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 72 ; i <= 85 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 89 ; i <= 93 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 106 ; i <= 129 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 141 ; i <= 149 ; i++){
    mvprintw(34,i,"-");
    }
    for ( int i = 6 ; i <= 20 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 22 ; i <= 49 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 56 ; i <= 72 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 85 ; i <= 99 ; i++){
    mvprintw(37,i,"-");
    }
     for ( int i = 106 ; i <= 120 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 125 ; i <= 129 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 136 ; i <= 139 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 141 ; i <= 149 ; i++){
    mvprintw(37,i,"-");
    }
    for ( int i = 6 ; i <= 15 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 19 ; i <= 44 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 51 ; i <= 69 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 85 ; i <= 93 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 101 ; i <= 112 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 121 ; i <= 132 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 138 ; i <= 143 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 147 ; i <= 149 ; i++){
    mvprintw(40,i,"-");
    }
    for ( int i = 6 ; i <= 41 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 47 ; i <= 49 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 54 ; i <= 67 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 75 ; i <= 89 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 103 ; i <= 110 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 118 ; i <= 124 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 133 ; i <= 139 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 141 ; i <= 149 ; i++){
    mvprintw(43,i,"-");
    }
    for ( int i = 9 ; i <= 25 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 31 ; i <= 47 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 58 ; i <= 62 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 64 ; i <= 89 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 95 ; i <= 122 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 124 ; i <= 129 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 131 ; i <= 140 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 142 ; i <= 144 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 146 ; i <= 149 ; i++){
    mvprintw(46,i,"-");
    }
    for ( int i = 6 ; i <= 12 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 15 ; i <= 32 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 34 ; i <= 48 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 51 ; i <= 69 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 72 ; i <= 83 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 88 ; i <= 94 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 96 ; i <= 114 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 118 ; i <= 126 ; i++){
    mvprintw(49,i,"-");
    }
    for ( int i = 130 ; i <= 149 ; i++){
    mvprintw(49,i,"-");
    }
     for ( int i = 6 ; i <= 9 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 16 ; i <= 32 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 41 ; i <= 48 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 58 ; i <= 69 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 78 ; i <= 83 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 93 ; i <= 94 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 104 ; i <= 114 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 127 ; i <= 126 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 140 ; i <= 149 ; i++){
    mvprintw(i,13,"|");
    }
    for ( int i = 6 ; i <= 10 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 21 ; i <= 30 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 39 ; i <= 46 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 57 ; i <= 66 ; i++){
    mvprintw(i,28,"|");
    }    
    for ( int i = 77 ; i <= 81 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 95 ; i <= 94 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 102 ; i <= 110 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 123 ; i <= 123 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 140 ; i <= 146 ; i++){
    mvprintw(i,28,"|");
    }
    for ( int i = 9 ; i <= 25 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 34 ; i <= 44 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 61 ; i <= 62 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 72 ; i <= 89 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 102 ; i <= 122 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 130 ; i <= 131 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 139 ; i <= 140 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 146 ; i <= 147 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 146 ; i <= 149 ; i++){
    mvprintw(i,43,"|");
    }
    for ( int i = 6 ; i <= 9 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 24 ; i <= 29 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 43 ; i <= 46 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 55 ; i <= 81 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 81 ; i <= 83 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 93 ; i <= 98 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 108 ; i <= 112 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 123 ; i <= 126 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 142 ; i <= 146 ; i++){
    mvprintw(i,58,"|");
    }
    for ( int i = 6 ; i <= 8 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 18 ; i <= 18 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 31 ; i <= 42 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 54 ; i <= 63 ; i++){
    mvprintw(i,73,"|");
    }
     for ( int i = 74 ; i <= 86 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 94 ; i <= 94 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 107 ; i <= 110 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 121 ; i <= 124 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 140 ; i <= 146 ; i++){
    mvprintw(i,73,"|");
    }
    for ( int i = 12 ; i <= 20 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 34 ; i <= 44 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 59 ; i <= 62 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 74 ; i <= 91 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 101 ; i <= 115 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 130 ; i <= 131 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 138 ; i <= 140 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 146 ; i <= 147 ; i++){
    mvprintw(i,88,"|");
    }
    for ( int i = 6 ; i <= 9 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 20 ; i <= 32 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 44 ; i <= 48 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 58 ; i <= 69 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 79 ; i <= 83 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 90 ; i <= 94 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 104 ; i <= 114 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 124 ; i <= 126 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 140 ; i <= 149 ; i++){
    mvprintw(i,103,"|");
    }
    for ( int i = 6 ; i <= 10 ; i++){
    mvprintw(i,118,"|");
    }
    for ( int i = 18 ; i <= 30 ; i++){
    mvprintw(i,118,"|");
    }
    for ( int i = 36 ; i <= 46 ; i++){
    mvprintw(i,188,"|");
    }
     for ( int i = 54 ; i <= 66 ; i++){
    mvprintw(i,188,"|");
    }
     for ( int i = 74 ; i <= 81 ; i++){
    mvprintw(i,188,"|");
    }
    for ( int i = 92 ; i <= 94 ; i++){
    mvprintw(i,188,"|");
    }
    for ( int i = 99 ; i <= 110 ; i++){
    mvprintw(i,118,"|");
    }
    for ( int i = 120 ; i <= 123 ; i++){
    mvprintw(i,118,"|");
    }
    for ( int i = 137 ; i <= 146 ; i++){
    mvprintw(i,118,"|");
    }
    for ( int i = 9 ; i <= 25 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 31 ; i <= 44 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 58 ; i <= 62 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 69 ; i <= 89 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 99 ; i <= 122 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 130 ; i <= 131 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 139 ; i <= 140 ; i++){
    mvprintw(i,133,"|");
    }
    for ( int i = 146 ; i <= 147 ; i++){
    mvprintw(i,133,"|");
    }
}

int main(){
    initscr();
    cbreak();
    noecho();
    keypad(stdscr,TRUE);
    int mv;
    int x = 9;
    int y = 9;
    int completo = 0;
    mapa();
    while(TRUE){
        personaje(y,x);
        mv = getch();
        if(x == 10 && y ==10){
            completo++;
            if(completo <=3)
                break;
        }
        if(mv == 'r'){
            x=8;
            y=8;
            if(completo == 0){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
            if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }

        }
         if(mv == 's'){
            x=8;
            y=8;
            completo = 1  ;

           if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa_III();
            }

        }

        if(mv == KEY_UP){
            y--;
            if(y<=5){
                y++;
            }
            if(completo == 0){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
            if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }

        }
        if(mv == KEY_DOWN){
            y++;
            if(y>=50){
                y--;
            }
            if(completo == 0){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
            if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
         }
         if(mv == KEY_RIGHT){
            x++;
            if(x>=150){
                x--;
            }
             if(completo == 0){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
            if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa();
           }

         }

         if(mv == KEY_LEFT){
            x--;
            if(x<=5){
                x++;
             if(completo == 0){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
            if(completo == 1){
                personaje(y,x);
                refresh();
                clear();
                mapa_I();
            }
            if(completo == 2){
                personaje(y,x);
                refresh();
                clear();
                mapa_II();
            }
            if(completo == 3){
                personaje(y,x);
                refresh();
                clear();
                mapa();
            }
        }

        if(mv == 'x' ){
            break;
        }
    } 
    refresh();
    endwin();
    return 0;
    }
}
}
